# Docker [![pipeline status](https://gitlab.com/zilliond/docker-images/badges/master/pipeline.svg)](https://gitlab.com/zilliond/docker-images/-/commits/master)
 
My docker images

## Commands for build and push to registry 
Autobuild with gitlab ci. 
```bash
# Use tag for registry name
docker build . -t registry.gitlab.com/zilliond/docker-images/php:latest
docker push registry.gitlab.com/zilliond/docker-images/php:latest
```
